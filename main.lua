local width = 10
local height = 30
local cell_size = 20

--[[
+------x
|
|
y

   0 1
0 [][]
1 [][]

  -1 0 1
-1  []
 0[][][]

  -1 0
0 [][]
1   []
2   []

   0 1 2 3
0 [][][][]

  0 1
0 []
1 [][]
2   []

  0 1
0   []
1 [][]
2 []

]]
math.randomseed(os.time())
local items = {
	{{0,0}, {0,1}, {1,0}, {1,1}},
	{{0,0}, {0,-1}, {-1,0}, {1,0}},
	{{0,0}, {-1,0}, {0,1}, {0, 2}},
	{{0,0}, {1,0}, {2,0}, {3, 0}},
	{{0,0}, {0,1}, {1,1}, {1, 2}},
	{{1,0}, {0,1}, {1,1}, {0, 2}}
}
local board = {}
local curr_item = {}

function clone_table(t)
	local ret = {}
	for k,v in pairs(curr_item) do
		if type(v)=='table' then
			ret[k] = clone_table(v)
		else 
			ret[k] = v
		end
	end
	return ret
end

function print_table(t)
	for k,v in pairs(t) do

		-- if type(v)=='table' then
		-- 	print_table(v)
		-- else
		-- 	print(v)
		-- end
	end
end

function love.load()
	local w = width * cell_size
	local h = (height+4) * (cell_size)
	love.window.setMode(w, h)
	love.graphics.setBackgroundColor(1,1,1,1)
	for x=0, width-1 do
		board[x] = {}
		for y=0, height-1 do
			board[x][y] = 0
		end
	end
end

local timer = 0
local falling = false
local game_over = false
local pause = false
local sorce = 0
local sorce_add = {100, 300, 600, 1000}

function do_drop(curr_item) -- return bool: can_drop
	-- 检测是否能向下move
	local can_move = false
	for k,v in pairs(curr_item) do
		can_move = true
		local x = v[1]
		local y = v[2]
		if board[x][y+1] == 1 or y+1>=height then
			can_move = false
			break
		end
	end

	if can_move then
		for k,v in pairs(curr_item) do
			-- 当前item向下移动一格
			local x = v[1]
			local y = v[2]
			curr_item[k] = {x, y+1}
		end
		return true
	else
		return false
	end
end

function love.update(dt)
	timer = timer + dt
	if game_over or timer<=0.3 then return end

	timer = 0
	if pause then return end


	if falling then -- 如果是下坠模式
		print('falling')
		falling = do_drop(curr_item)

		if not falling then
			for k,v in pairs(curr_item) do
				local x = v[1]
				local y = v[2]
				board[x][y] = 1
			end
		end

	else -- 不是下坠模式
		print('not falling')
		-- 随机一个item，判断游戏是否输了
		local idx = math.random(1, 6)
		local t = items[idx]
		for k,v in pairs(t) do
			local x = v[1]+5
			local y = v[2]
			if board[x][y] == 1 then
				game_over = true
			end
			curr_item[k] = {x, y}
		end
		falling = true

	end

	-- 检测并消除满了的行
	local all_1_cnt = 0;
	for y=0, height-1 do
		local all_1 = true
		for x=0, width-1 do
			if board[x][y] == 0 then
				all_1 = false
			end
		end
		if all_1 then
			all_1_cnt = all_1_cnt + 1
			for x=0,width-1 do
				for i=y,1,-1 do
					board[x][i] = board[x][i-1]
				end
			end
		end
	end
	if all_1_cnt >= 1 and all_1_cnt <=4 then
		sorce = sorce + sorce_add[all_1_cnt]
	end

	if game_over then
		for x=0,width-1 do
			for y=0,height-1 do
				board[x][y] = 0
			end
		end
		game_over = false
	end
end

function love.keypressed(key)
	local key_to_x = {
		["left"] = -1,
		["a"] = -1,
		["A"] = -1,
		["right"] = 1,
		["d"] = 1,
		["D"] = 1,
	}
	if key_to_x[key] ~= nil then
		local _x = key_to_x[key]
		local can_move = true
		for k,v in pairs(curr_item) do
			local x = v[1]
			local y = v[2]
			local next_x = x + _x
			local next_y = y
			-- if next_x < 0 or next_x >= width then
			if next_x<0 or next_x>=width
			   or next_y<0 or next_y>=height
			   or board[next_x][next_y]==1 then
				can_move = false
			end
		end

		if can_move then
			for k,v in pairs(curr_item) do
				local x = v[1]
				local y = v[2]
				local next_x = x + _x
				
				curr_item[k] = {next_x, y}
			end
		end
	end

	if key=='up' or key=='w' or key=='W' then

		local midx = curr_item[1][1]
		local midy = curr_item[1][2]
		local can_move = true
		for k,v in pairs(curr_item) do
			local x = v[1] - midx
			local y = v[2] - midy
			local next_x = midx - y
			local next_y = midy + x
			if next_x<0 or next_x>=width
			   or next_y<0 or next_y>=height
			   or board[next_x][next_y]==1 then
			   	can_move = false
			end
		end
		if can_move then
			for k,v in pairs(curr_item) do
				local x = v[1] - midx
				local y = v[2] - midy
				local next_x = midx - y
				local next_y = midy + x
				curr_item[k] = {next_x, next_y}
			end
		end

	elseif key=='down' or key=='s' or key=='S' then

		while do_drop(curr_item) do
			
		end

	elseif key=='p' or key=='P' or key=='escape' then

		pause = not pause


	end

end

function show_item(item, r, g, b, a)

	for k,v in pairs(item) do
		local x = v[1]
		local y = v[2]
		love.graphics.setColor(r, g, b, a)
		love.graphics.rectangle("fill", x*cell_size, (y+4)*cell_size, cell_size, cell_size)
	end
end

function love.draw()

	love.graphics.print("" .. sorce) 

	if game_over then

		--todo: show "game over!"

	else

		-- love.graphics.d
		love.graphics.setColor(0,0,0,1)
		love.graphics.rectangle("fill", 0, cell_size*4-10, cell_size*width, 10)
		for x=0, width-1 do
			for y=0, height-1 do
				if board[x][y]==1 then
					love.graphics.setColor(0/255, 0/255, 0/255, 1)
				else
					love.graphics.setColor(255/255, 255/255, 255/255, 1)
				end
				love.graphics.rectangle("fill", x*cell_size, (y+4)*cell_size, cell_size, cell_size)
			end
		end


		local forecast = {}
		for k,v in pairs(curr_item) do
			local x = v[1]
			local y = v[2]
			forecast[k] = {x, y}
		end
		while do_drop(forecast) do end
		show_item(forecast, 200/255, 200/255, 200/255, 1)

		show_item(curr_item, 0/255, 0/255, 0/255, 1)

	end
end
